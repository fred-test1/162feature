#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=missing-function-docstring
"""
@Description  : routes.py -
@Date         : 2020/11/06 22:29:02
@Author       : Ruijie Dong
@Version      : 1.0
"""
import json
import math
import smtplib
import datetime
from queue import Queue
from threading import Timer
from email.mime.text import MIMEText

from flask import abort
from flask import jsonify
from flask import request
from flask import make_response, render_template, url_for, redirect

from app import app

# size of the queue for keeping alert messages
LOG_QUEUE_SIZE = 20000
# number of logs to show per page
MSG_NUMBER_PER_PAGE = 100

# size of the queue for keeping alert messages
ALERT_QUEUE_SIZE = 5000

SEND_LOGS_TIMER = 10

log_q = dict()
alert_q = Queue(maxsize=ALERT_QUEUE_SIZE)

@app.errorhandler(404)
def not_found():
    return make_response(jsonify({'error': 'Not found'}), 404)


@app.route('/')
@app.route('/index')
def index():
    return "Hello from Flask!"


@app.route('/logs/<appname>')
@app.route('/logs/<appname>/<int:page>')
def show_logs(appname: str, page=1):
    if appname not in log_q.keys():
        return 'No log message for %s.' % appname

    if page<1:
        return redirect(url_for('show_logs', appname=appname, page=1))
    log_size = len(log_q[appname])
    max_page = math.ceil(log_size/MSG_NUMBER_PER_PAGE)
    if page>max_page:
        return redirect(url_for('show_logs', appname=appname, page=max_page))

    end_index = log_size - (page-1)*MSG_NUMBER_PER_PAGE
    start_index = log_size - page*MSG_NUMBER_PER_PAGE
    if start_index<0:
        start_index=0

    # messages = list(log_q[appname])
    # for msg in log_q[appname]:
    #     messages.append(msg)
    return render_template("log_messages.html",
        appname = appname,
        page = page,
        messages = log_q[appname][start_index:end_index])


@app.route('/logs', methods=['GET'])
@app.route('/logs/', methods=['GET'])
def show_applist():
    appname = request.args.get('appname')
    if appname is not None:
        return redirect(url_for('show_logs', appname=appname))
    else:
        applist = log_q.keys()
        return render_template("log_index.html", applist = applist)


@app.route('/logs', methods=['POST'])
def receive_logs():
    if not request.json:
        abort(400)
    message = "%s [%s] [%s] [%d:%s] [%s:%d] %s " % \
            (request.json['name'],
            request.json['asctime'],
            request.json['levelname'],
            request.json['process'],
            request.json['processName'],
            request.json['filename'],
            request.json['lineno'],
            request.json['message'])
    # save message to log_q
    appname = request.json['name']
    # if appname not in log_q.keys():
    #     log_q[appname] = deque(maxlen=LOG_QUEUE_SIZE)
    # log_q[appname].append(request.json)
    if appname not in log_q.keys():
        log_q[appname] = []
    log_q[appname].append(request.json)
    while len(log_q[appname]) > LOG_QUEUE_SIZE:
        log_q[appname].pop(0)

    # save message to alert_q
    if request.json['levelname'] in ['WARNING','ERROR','CRITICAL','FATAL']:
        alert_q.put(message)
    # is_done = send_email(alert_message)
    return jsonify({'result': 'Done'}), 201


def send_logs(inc):
    # print('sent alerts ...')
    log_msg = {"name" : "TestApp1", "asctime" : datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), "levelname":"INFO",
            "process": "1001", "proccessName":"Main Process", "filename":"test.py", "lineno": "10", "message":"test message"}
    msg_json = json.dumps(log_msg) # Encode the data
    log_q['TestApp1'].append(log_msg)
    while len(log_q['TestApp1']) > LOG_QUEUE_SIZE:
        log_q[appname].pop(0)

    log_msg = {"name" : "TestApp2", "asctime" : datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), "levelname":"INFO",
            "process": "2001", "proccessName":"Main Process", "filename":"test.py", "lineno": "10", "message":"test message"}
    msg_json = json.dumps(log_msg) # Encode the data
    log_q['TestApp2'].append(log_msg)
    while len(log_q['TestApp2']) > LOG_QUEUE_SIZE:
        log_q[appname].pop(0)


    timer = Timer(inc, send_logs, (inc,))
    timer.start()

log_q['TestApp1'] = []
log_q['TestApp2'] = []
send_logs(SEND_LOGS_TIMER)
