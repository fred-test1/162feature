# For more information, please refer to https://aka.ms/vscode-docker-python
ARG CI_REGISTRY_IMAGE='library'
FROM ${CI_REGISTRY_IMAGE}/python:3.8.5-alpine

ARG PIP_INDEX_URL='https://pypi.python.org/simple'
ENV PIP_INDEX_URL=${PIP_INDEX_URL}

# Keeps Python from generating .pyc files in the container
ENV PYTHONDONTWRITEBYTECODE 1

# Turns off buffering for easier container logging
ENV PYTHONUNBUFFERED 1

# export PATHONPATH
ENV PYTHONPATH "${PYTHONPATH}:/app"

WORKDIR /app
ADD . /app

# Install pip requirements
RUN python -m pip install --index-url ${PIP_INDEX_URL} -r /app/requirements.txt
RUN mkdir -p /app/log

# Switching to a non-root user, please refer to https://aka.ms/vscode-docker-python-user-rights
RUN adduser -D appuser && chown -R appuser /app
USER appuser

EXPOSE 5000

# During debugging, this entry point will be overridden. For more information, please refer to https://aka.ms/vscode-docker-python-debug
CMD ["gunicorn", "--config", "/app/config.py", "run:app"]
