# Python项目演示

## 1. 项目简介
- 该项目演示一个Python Web程序在GitLab中的构建、打包和安全扫描全过程
- Pipeline主要包括下面几个stages：
  - build：构建、打包Python Web程序，推送到GitLab自带的容器镜像仓库；
  - test：静态扫描、依赖扫描、容器扫描、License扫描
  - dast：在容器网络内部发布该程序、并执行动态扫描

## 2. 分支说明
- master：可连通外网的场景演示
- gitlab-package-registry：不能连通外网的场景，使用GitLab自带的容器镜像仓库和软件包仓库
- artifactory：不能连通外网的场景，使用GitLab自带的容器镜像仓库和Artifactory软件包仓库
- nexus：不能连通外网的场景，使用GitLab自带的容器镜像仓库和Nexus软件包仓库

## 3. 环境准备
### 3.1 master分支
- 确保能访问下列地址
  - registry.gitlab.com
  - docker.io
  - github.com
  - gitlab.com
  - python.org

### 3.2 gitlab-package-registry分支
- 从gitlab.com中导入安全扫描依赖项目
  - [gemnasium-db](https://gitlab.com/gitlab-org/security-products/gemnasium-db)
- Push Docker镜像到GitLab Container Registry
  - 所有安全扫描镜像
  - docker:19.03.12
  - docker:19.03.12-dind
  - python:3.8.5-alpine
- 上传项目依赖的PyPi软件包到GitLab Pakcage Registry
  - 创建Job Token：Settings -> Access Token
  - 创建~/.pypirc
  - 上传pypi软件包：python3 -m twine upload --repository gitlab dist/\*
- 修改.gitlab-ci.yml环境变量
  - PIP_INDEX_URL
  - SECURE_ANALYZERS_PREFIX
  - GEMNASIUM_DB_REMOTE_URL

~/.pypirc示例:
```yaml
[distutils]
index-servers =
    gitlab

[gitlab]
repository = https://gitlab.cdneks.com/api/v4/projects/15/packages/pypi
username = gitlab-ci-token
password = YOUR_TOKEN
```

### 3.3 artifactory分支
- 从gitlab.com中导入安全扫描依赖项目
  - [gemnasium-db](https://gitlab.com/gitlab-org/security-products/gemnasium-db)
- Push Docker镜像到GitLab Container Registry
  - 所有安全扫描镜像
  - docker:19.03.12
  - docker:19.03.12-dind
  - python:3.8.5-alpine
- 在Artifactory中创建一个remote类型的PyPi仓库
- 修改.gitlab-ci.yml环境变量
  - PIP_INDEX_URL
  - SECURE_ANALYZERS_PREFIX
  - GEMNASIUM_DB_REMOTE_URL

### 3.4 nexus分支
- 从gitlab.com中导入安全扫描依赖项目
  - [gemnasium-db](https://gitlab.com/gitlab-org/security-products/gemnasium-db)
- Push Docker镜像到GitLab Container Registry
  - 所有安全扫描镜像
  - docker:19.03.12
  - docker:19.03.12-dind
  - python:3.8.5-alpine
- 在Nexus中创建一个proxy类型的PyPi仓库
- 修改.gitlab-ci.yml环境变量
  - PIP_INDEX_URL
  - SECURE_ANALYZERS_PREFIX
  - GEMNASIUM_DB_REMOTE_URL

